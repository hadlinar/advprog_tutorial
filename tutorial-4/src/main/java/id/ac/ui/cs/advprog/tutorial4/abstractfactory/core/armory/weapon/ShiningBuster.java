package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon;

public class ShiningBuster implements Weapon {

    @Override
    public String getName() {
        return "Shining Buster weapon";
    }

    @Override
    public String getDescription() {
        return "This is shining buster weapon";
    }
}
