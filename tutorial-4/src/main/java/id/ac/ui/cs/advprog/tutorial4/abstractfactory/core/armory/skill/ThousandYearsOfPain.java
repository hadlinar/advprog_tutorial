package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill;

public class ThousandYearsOfPain implements Skill {

    @Override
    public String getName() {
        return "Thousand Years of Pain force";
    }

    @Override
    public String getDescription() {
        return "This is thousand years of pain force skill";
    }
}
