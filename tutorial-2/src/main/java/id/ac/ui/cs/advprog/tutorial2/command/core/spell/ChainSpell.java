package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    private  ArrayList<Spell> spell;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spell = spells;
    }

    @Override
    public void cast() {
        for(Spell s: spell){
            s.cast();
        }
    }

    @Override
    public void undo() { //urutannya kebalik???
        for (int i=spell.size()-1; i>0; i--){
            spell.get(i).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
