package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                this.guild.add(this);
        }

        @Override
        public void update() {
                if(this.guild.getQuestType().equalsIgnoreCase("D") || this.guild.getQuestType().equalsIgnoreCase("E"));
                this.getQuests().add(this.guild.getQuest());
                System.out.println("Quest dijalankan oleh " + this.name);
        }
}
