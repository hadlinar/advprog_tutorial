package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild;
                this.guild.add(this);
        }

        @Override
        public void update() {
                if(this.guild.getQuestType().equalsIgnoreCase("D") || this.guild.getQuestType().equalsIgnoreCase("R")){
                        this.getQuests().add(this.guild.getQuest());
                        System.out.println("Quest dijalankan oleh " + this.name);
                }
        }
}
